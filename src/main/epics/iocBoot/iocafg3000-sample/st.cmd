#!../../bin/linux-x86_64/afg3000-sample

< envPaths
< envUser
< envSystem

# A bug in current maven plugin doesn't allow environment variables...
epicsEnvSet("AFG_IP",    "$(AFG3000_IP=10.4.3.200)")   # Choose afg ethernet address
epicsEnvSet("AFG_PREFIX", "$(AFG3000_PREFIX=AFG3102C)") # Choose site prefix name


epicsEnvSet("AFG_ASYN_PORT",  "AFG3102C")  # Choose asyn port name

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", 100000)
epicsEnvSet("STREAM_PROTOCOL_PATH",     "${CODAC_ROOT}/epics/protocol")

cd "${TOP}"

#############################################
## Register all support components         ##
#############################################

dbLoadDatabase "dbd/afg3000-sample.dbd"
afg3000_sample_registerRecordDeviceDriver pdbbase

# Setup IOC->hardware link
vxi11Configure("$(AFG_ASYN_PORT)", "$(AFG_IP)", 0, 0.0, "inst0", 0)

# Load records
dbLoadRecords("$(EPICS_MODULES)/afg3000/db/AFG3102C.db","PREFIX=$(AFG_PREFIX),ASYN_PORT=$(AFG_ASYN_PORT),ASYN_ADDR=0")

# Enable asyn trace
asynSetTraceMask($(AFG_ASYN_PORT),0, 0x9)
asynSetTraceIOMask($(AFG_ASYN_PORT),0, 0x2)

#############################################
## IOC Logging                             ##
#############################################
iocLogInit

#############################################
## IOC initialization                      ##
#############################################
cd "${TOP}/db"
iocInit
